package com.o22.taskmanager.model;

import java.util.HashSet;

/**
 * Created by Александр on 25.10.2015.
 */
public class Journal implements IJournal{
    private HashSet<Task> Tasks;

    private Journal(){
    }
    
    public void addTask(Task task){
        if(Tasks==null)
            Tasks=new HashSet<Task>();
        Tasks.add(task);
    }
    public HashSet<Task> getTasks(){
        return Tasks;
    }
    public boolean deleteTask(Task task){
        return Tasks.remove(task);
    }
    public void setJournal(HashSet<Task> tasks){
        this.Tasks = tasks;
    }
}
