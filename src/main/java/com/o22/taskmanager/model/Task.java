package com.o22.taskmanager.model;

import java.io.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Nastya on 25.10.2015.
 */
public class Task implements Serializable{
    private String name;
    private String description;
    private Calendar date;
    private String contacts;

    public Task(){
        name = "New task";
        description = "-";
        date = new GregorianCalendar();
        contacts = "89179720354";
    }
    public Task(String name, String description, Calendar date, String contacts) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.contacts = contacts;
    }
    public Task(String name, Calendar date){
        this.name=name;
        this.date=date;
        this.date.set(Calendar.MONTH,date.get(date.MONTH)-1);
        description="-";
        contacts="89179720354";
    }

    public String getName() {
        return name;
    }
    public String getDescription() {
        return description;
    }
    //    public Calendar getDate() {
//        return date;
//    }
    public String getContacts() {
        return contacts;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public void setDate(int year, int month, int day, int hour, int minute, int second){
        this.date=new GregorianCalendar(year,month,day,hour,minute,second);
    }
    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getDate(){
        int year = date.get(date.YEAR);
        int month = date.get(date.MONTH)+1;
        int day = date.get(date.DATE);
        int hour = date.get(date.HOUR);
        int minute = date.get(date.MINUTE);
        int second = date.get(date.SECOND);
        return year+"."+month+"."+day+" "+hour+":"+minute+":"+second;
    }
    /*
    public static void outputTask(Task task, OutputStream out)throws IOException{
-        DataOutputStream output = new DataOutputStream(out);
-        output.writeInt(task.id);
-        output.writeUTF(task.name);
-        output.writeUTF(task.description);
-        output.writeInt(task.date.get(task.date.YEAR));
-        output.writeInt(task.date.get(task.date.MONTH) + 1);
-        output.writeInt(task.date.get(task.date.DATE));
-        output.writeInt(task.date.get(task.date.HOUR));
-        output.writeInt(task.date.get(task.date.MINUTE));
-        output.writeInt(task.date.get(task.date.SECOND));
-        output.writeUTF(task.contacts);
-        output.close();
-    }
-    public static Task inputTask(InputStream in) throws IOException {
-        DataInputStream input = new DataInputStream(in);
-        int id = input.readInt();
-        String name = input.readUTF();
-        String description = input.readUTF();
-        int year = input.readInt();
-        int month = input.readInt();
-        int day = input.readInt();
-        int hour = input.readInt();
-        int minute = input.readInt();
-        int second = input.readInt();
-        String contacts = input.readUTF();
-        return new Task(id,name,description, new GregorianCalendar(year,month,day,hour,minute,second),contacts);
-    }
     */
    public Calendar getCalendarDate(){
        return date;
    }
    @Override
    public String toString(){
        return name+" "+description+" "+getDate()+" "+contacts;
          }
}