package com.o22.taskmanager.view;


import com.o22.taskmanager.exception.InvalidDataException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import com.o22.taskmanager.model.Task;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditTaskView {

    private TextField nameField;
    private TextArea descField;
    private DatePicker datePicker;
    private TextField timeField;
    private TextField contactsField;

    private Task task;

    public void display() {
        final Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("com.o22.taskmanager.model.Task editing");

        GridPane pane = new GridPane();
        pane.setPadding(new Insets(10));
        pane.setHgap(10);
        pane.setVgap(5);

        Label nameLabel = new Label();
        nameLabel.setText("Enter taskname");

        nameField = new TextField();
        nameField.setMaxWidth(250);

        Label descLabel = new Label();
        descLabel.setText("Enter description");

        descField = new TextArea();
        descField.setMaxWidth(250);

        Label dateLabel = new Label();
        dateLabel.setText("Select date");

        datePicker = new DatePicker(LocalDate.now());
        datePicker.setMinWidth(250);

        Label timeLabel = new Label();
        timeLabel.setText("Enter time (hh:mm)");

        timeField = new TextField();
        timeField.setMaxWidth(250);

        Label contactsLabel = new Label();
        contactsLabel.setText("Enter contacts");

        contactsField = new TextField();
        contactsField.setMaxWidth(250);

        Button button = new Button();
        button.setText("Add task");

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    if (validate()) {
                        String name = nameField.getText();
                        String desc = descField.getText();
                        LocalDate date = datePicker.getValue();
                        LocalTime time = LocalTime.parse(timeField.getText());
                        //task = new com.o22.taskmanager.model.Task(name, desc, date, time);
                        stage.close();
                    }
                } catch (InvalidDataException e) {
                    AlertBox.display("Invalid data", "Enter valid data");
                } catch (DateTimeParseException e) {
                    AlertBox.display("Invalid time", "Enter valid time");
                }
            }
        });


        pane.add(nameLabel, 0, 0);
        pane.add(nameField, 1, 0);
        pane.add(descLabel, 0, 1);
        GridPane.setValignment(descLabel, VPos.TOP);
        pane.add(descField, 1, 1);
        pane.add(dateLabel, 0, 2);
        pane.add(datePicker, 1, 2);
        pane.add(timeLabel, 0, 3);
        pane.add(timeField, 1, 3);
        pane.add(contactsLabel, 0, 4);
        pane.add(contactsField, 1, 4);
        GridPane.setMargin(button, new Insets(20, 0, 0, 0));
        pane.add(button, 0, 5);

        Scene scene = new Scene(pane, 380, 350);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.showAndWait();
    }

    private boolean validate() throws InvalidDataException {
        if (nameField.getText().isEmpty())
            throw new InvalidDataException();
        if (descField.getText().isEmpty())
            throw new InvalidDataException();
        if (datePicker.getValue() == null)
            throw new InvalidDataException();
        Pattern pattern = Pattern.compile("^\\d{2}:\\d{2}$");
        Matcher matcher = pattern.matcher(timeField.getText());
        if (!matcher.matches())
            throw new InvalidDataException();
        return true;
    }

    public Task getTask() {
        return task;
    }
}
