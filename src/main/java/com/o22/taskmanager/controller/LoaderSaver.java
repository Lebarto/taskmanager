package com.o22.taskmanager.controller;

import com.o22.taskmanager.model.Task;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Александр on 01.11.2015.
 */
public class LoaderSaver implements ILoaderSaver {
    LoaderSaver(){}
    @Override
    public Set<Task> loadJournal() {
        HashSet<Task> tasks=null;
        try {
            FileInputStream fis = new FileInputStream("save.tm");
            ObjectInputStream oin = new ObjectInputStream(fis);
            tasks = (HashSet<Task>) oin.readObject();
        }
        catch (IOException e){
            //LOG.error("IOException");

        }
        catch (ClassNotFoundException c){
            //LOG.error("ClassNotFoundException");
        }
        return tasks;

    }
    @Override
    public void saveTask(Set<Task> tasks) {
        try {
            FileOutputStream fos = new FileOutputStream("save.tm");
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(tasks);
            oos.flush();
            oos.close();
        }
        catch (IOException e){
            //LOG.error("IOException");
        }
    }
}
