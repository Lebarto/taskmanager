package com.o22.taskmanager.controller;

import java.awt.event.ActionListener;
import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;


import com.o22.taskmanager.model.Journal;
import com.o22.taskmanager.model.Task;
import com.o22.taskmanager.view.AlertBox;
import javafx.application.Platform;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import java.awt.event.*;

/**
 * Created by Александр on 25.10.2015.
 */
public class Controller implements IController {

    //public static final Logger LOG=Logger.getLogger(Controller.class);
    Journal journal;
    ILoaderSaver ls;
    public Controller() throws IOException, ClassNotFoundException {
        ApplicationContext genericXmlApplicationContext;
        genericXmlApplicationContext = new GenericXmlApplicationContext("app-context.xml");
        ls=(LoaderSaver)genericXmlApplicationContext.getBean("loadersaver");

        journal=(Journal)genericXmlApplicationContext.getBean("journal");
        journal.setJournal((HashSet<Task>)ls.loadJournal());


        startTimers();
    }
    @Override
    public void addTask(String name, Calendar date, String contacts, String description) {
        Task t = new Task(name,description,date,contacts);
        journal.addTask(t);
        Save();
        startTimers();

    }
    public void addTask(String name,String description,LocalDate ld,LocalTime lt,String contacts){
        LocalDate locd=ld;
        LocalTime loct=lt;
        Calendar calendar = new GregorianCalendar(ld.getYear(),ld.getMonthValue(),ld.getDayOfMonth(),lt.getHour(),lt.getMinute());
        journal.addTask(new Task(name,description,calendar,contacts));
        startTimers();

    }

    @Override
    public void deleteTask(Task task) {
        Task t = new Task();
        journal.deleteTask(t);
    }
    public void Save(){
        ls.saveTask(journal.getTasks());
    }
    public void startTimers() {
        final Calendar cal = new GregorianCalendar();

        Timer[] timers = new Timer[journal.getTasks().size()];



        int i = 0;
        System.out.println(Calendar.getInstance().getTime());
        for (final Task t : journal.getTasks()) {

            final String title = t.getName();
            final String desc = t.getDescription();
            TimerTask task=new TaskTimer(t);
            timers[i]=new Timer();
            Date today = Calendar.getInstance().getTime();
            timers[i].schedule(task,t.getCalendarDate().getTime().getTime()-today.getTime());
            System.out.println((t.getCalendarDate().getTime().getTime() - today.getTime()) / 1000 + "   " + t.toString());
            i++;
        }
    }

    @Override
    public HashSet<Task> getJournal(){
        return journal.getTasks();
    }



}
class TaskTimer extends TimerTask{
    Task task;

    TaskTimer(Task t) {
        task=t;
    }
    @Override
    public void run() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                AlertBox.display(task.getName(), task.getDescription());
            }


        });

    }
}
